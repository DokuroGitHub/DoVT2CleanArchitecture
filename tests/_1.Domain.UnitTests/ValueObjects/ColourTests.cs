﻿using Domain.Exceptions;
using Domain.ValueObjects;
using FluentAssertions;
using NUnit.Framework;

namespace Domain.UnitTests.ValueObjects;

public class ColourTests
{
    [Test]
    public void ShouldReturnCorrectColourCode()
    {
        // Arrange
        var code = "#FFFFFF";

        // Act
        var colour = Colour.From(code);

        // Assert
        colour.Code.Should().Be(code);
    }

    [Test]
    public void ToStringReturnsCode()
    {
        // Arrange
        var colour = Colour.White;

        // Act & Assert
        colour.ToString().Should().Be(colour.Code);
    }

    [Test]
    public void ShouldPerformImplicitConversionToColourCodeString()
    {
        // Arrange
        string code = Colour.White;

        // Act & Assert
        code.Should().Be("#FFFFFF");
    }

    [Test]
    public void ShouldPerformExplicitConversionGivenSupportedColourCode()
    {
        // Arrange
        var colour = (Colour)"#FFFFFF";

        // Act & Assert
        colour.Should().Be(Colour.White);
    }

    [Test]
    public void ShouldThrowUnsupportedColourExceptionGivenNotSupportedColourCode()
    {
        // Act & Assert
        FluentActions.Invoking(() => Colour.From("##FF33CC"))
            .Should().Throw<UnsupportedColourException>();
    }
}
