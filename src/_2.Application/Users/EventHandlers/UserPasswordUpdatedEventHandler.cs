﻿using Domain.Events;
using MediatR;
using Microsoft.Extensions.Logging;

namespace Application.Users.EventHandlers;

public class UserPasswordUpdatedEventHandler : INotificationHandler<UserPasswordUpdatedEvent>
{
    private readonly ILogger<UserPasswordUpdatedEventHandler> _logger;

    public UserPasswordUpdatedEventHandler(ILogger<UserPasswordUpdatedEventHandler> logger)
    {
        _logger = logger;
    }

    public Task Handle(UserPasswordUpdatedEvent notification, CancellationToken cancellationToken)
    {
        _logger.LogInformation("CleanArchitecture Domain Event: {DomainEvent}", notification.GetType().Name);

        return Task.CompletedTask;
    }
}
