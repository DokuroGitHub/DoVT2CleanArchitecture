﻿using Application.Common.Interfaces.IRepositories;
using Domain.Common;
using Microsoft.EntityFrameworkCore;

namespace Application.Common.Interfaces;

public interface IUnitOfWork : IDisposable
{
    // repositories
    public ITodoListRepository TodoListRepository { get; }
    public ITodoItemRepository TodoItemRepository { get; }
    public IUserRepository UserRepository { get; }

    // state
    EntityState State<TEntity>(TEntity entity) where TEntity : BaseEntity;

    // save changes
    // int SaveChanges(); // not using bc mediatR events need async
    // Task<int> SaveChangesAsync();
    Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    // transaction
    void BeginTransaction();
    // commit
    void Commit();
    Task CommitAsync();
    // rollback
    void Rollback();
    Task RollbackAsync();
    // execute transaction
    Task ExecuteTransactionAsync(Action action);
    Task<T> ExecuteTransactionAsync<T>(Func<T> action);
}
