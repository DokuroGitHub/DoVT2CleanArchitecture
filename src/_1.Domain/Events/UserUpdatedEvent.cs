﻿namespace Domain.Events;

public class UserUpdatedEvent : BaseEvent
{
    public UserUpdatedEvent(User item)
    {
        Item = item;
    }

    public User Item { get; }
}
