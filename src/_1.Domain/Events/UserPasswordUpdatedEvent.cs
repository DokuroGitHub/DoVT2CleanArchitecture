﻿namespace Domain.Events;

public class UserPasswordUpdatedEvent : BaseEvent
{
    public UserPasswordUpdatedEvent(User item)
    {
        Item = item;
    }

    public User Item { get; }
}
