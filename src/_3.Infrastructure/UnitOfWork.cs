
using System.Transactions;
using Application.Common.Interfaces;
using Application.Common.Interfaces.IRepositories;
using AutoMapper;
using Domain.Common;
using Infrastructure.Persistence;
using Infrastructure.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

namespace Infrastructure;

public class UnitOfWork : IUnitOfWork
{
    private IDbContextTransaction? _transaction;
    private bool _disposed;
    //
    private readonly ApplicationDbContext _context;
    //
    private readonly ITodoListRepository _todoListRepository;
    private readonly ITodoItemRepository _todoItemRepository;
    private readonly IUserRepository _userRepository;

    // repositories
    public ITodoListRepository TodoListRepository => _todoListRepository;
    public ITodoItemRepository TodoItemRepository => _todoItemRepository;
    public IUserRepository UserRepository => _userRepository;
    //
    public UnitOfWork(ApplicationDbContext dbContext, IMapper mapper)
    {
        _context = dbContext;
        // repositories
        _todoListRepository = new TodoListRepository(_context, mapper);
        _todoItemRepository = new TodoItemRepository(_context, mapper);
        _userRepository = new UserRepository(_context, mapper);
    }

    // state
    public EntityState State<TEntity>(TEntity entity) where TEntity : BaseEntity
        => _context.Entry<TEntity>(entity).State;

    // save changes
    // public int SaveChanges() => _context.SaveChanges();

    // public async Task<int> SaveChangesAsync() => await _context.SaveChangesAsync();

    public async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        => await _context.SaveChangesAsync(cancellationToken);

    // transaction
    public void BeginTransaction()
    {
        _transaction = _context.Database.BeginTransaction();
    }

    // commit
    public void Commit()
    {
        if (_transaction is null)
            throw new TransactionException("No transaction to commit");
        try
        {
            _context.SaveChanges();
            _transaction.Commit();
        }
        finally
        {
            _transaction.Dispose();
            _transaction = null;
        }
    }

    public async Task CommitAsync()
    {
        if (_transaction is null)
            throw new TransactionException("No transaction to commit");
        try
        {
            await _context.SaveChangesAsync();
            _transaction.Commit();
        }
        finally
        {
            _transaction.Dispose();
            _transaction = null;
        }
    }

    // rollback
    public void Rollback()
    {
        if (_transaction is null)
            throw new TransactionException("No transaction to rollback");
        _transaction.Rollback();
        _transaction.Dispose();
        _transaction = null;
    }

    public async Task RollbackAsync()
    {
        if (_transaction is null)
            throw new TransactionException("No transaction to rollback");
        await _transaction.RollbackAsync();
        _transaction.Dispose();
        _transaction = null;
    }

    // dispose
    protected virtual void Dispose(bool disposing)
    {
        if (!_disposed)
        {
            if (disposing)
            {
                _transaction?.Dispose();
                _context.Dispose();
            }

            _disposed = true;
        }
    }

    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    // execute transaction
    public async Task ExecuteTransactionAsync(Action action)
    {
        using var transaction = await _context.Database.BeginTransactionAsync();
        try
        {
            action();
            // action.Invoke();
            await _context.SaveChangesAsync();
            await transaction.CommitAsync();
        }
        catch (Exception ex)
        {
            await transaction.RollbackAsync();
            throw new TransactionException("Could not execute transaction", ex);
        }
    }

    public async Task<T> ExecuteTransactionAsync<T>(Func<T> action)
    {
        using var transaction = await _context.Database.BeginTransactionAsync();
        try
        {
            var result = action();
            await _context.SaveChangesAsync();
            await transaction.CommitAsync();
            return result;
        }
        catch (Exception ex)
        {
            await transaction.RollbackAsync();
            throw new TransactionException("Could not execute transaction", ex);
        }
    }
}
