# Clean Architecture

```bash
# Change DefaultConnection at src._4.Api.appsettings.Development.json.ConnectionStrings.DefaultConnection

cd src

# migration
dotnet ef migrations add InitDb -p "_3.Infrastructure" -s "_4.Api"

dotnet ef database update -p "_3.Infrastructure" -s "_4.Api"

cd "_4.Api"
dotnet watch

#
dotnet new gitignore
git init
git add .
git commit -m "c1 Init"
git push --set-upstream https://gitlab.com/DokuroGitHub/DoVT2CleanArchitecture.git master

```
